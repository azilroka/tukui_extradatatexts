local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local MyRealm = EDT.MyRealm
local MyName = EDT.MyName

local tinsert, sort, pairs, tonumber = tinsert, sort, pairs, tonumber
local format, gsub, strfind, select, floor = format, gsub, strfind, select, floor
local GetSpecialization, GetSpecializationInfo, SetLootSpecialization, UseEquipmentSet = GetSpecialization, GetSpecializationInfo, SetLootSpecialization, UseEquipmentSet
local IsInInstance, GetLootSpecialization, GetSpecializationInfoByID, GetEquipmentSetInfoByName = IsInInstance, GetLootSpecialization, GetSpecializationInfoByID, GetEquipmentSetInfoByName
local GetActiveSpecGroup, SetActiveSpecGroup, GetEquipmentSetItemIDs, GetEquipmentSetLocations = GetActiveSpecGroup, SetActiveSpecGroup, GetEquipmentSetItemIDs, GetEquipmentSetLocations
local EquipmentManager_UnpackLocation, GetContainerItemDurability, GetInventoryItemDurability, GetItemInfo, GetInventoryItemLink = EquipmentManager_UnpackLocation, GetContainerItemDurability, GetInventoryItemDurability, GetItemInfo, GetInventoryItemLink
local GearSets, LootTable = {}, {}

local function GetGearSets(set)
	wipe(GearSets)

	GearSets[1] = { text = 'None', checked = function() return TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set] == 'None' end, func = function() TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set] = 'None' end }

	if GetNumEquipmentSets() ~= 0 then
		for i = 1, GetNumEquipmentSets() do
			local name = GetEquipmentSetInfo(i)
			if name then
				tinsert(GearSets, { text = name, checked = function() return TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set] == name end, func = function() TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set] = name end })
			end
		end
	end

	return GearSets
end

local function LootSpecialization()
	local SpecIndex = GetSpecialization()

	wipe(LootTable)

	if SpecIndex then
		local SpecID, SpecName = GetSpecializationInfo(SpecIndex)
		LootTable[1] = { text = format(LOOT_SPECIALIZATION_DEFAULT, SpecName), notCheckable = true, func = function() SetLootSpecialization(0) end }

		for Index = 1, 4 do
			local ID, Name = GetSpecializationInfo(Index)
			if ID then
				tinsert(LootTable, { text = Name, notCheckable = true, func = function() SetLootSpecialization(ID) end })
			end
		end
	end

	return LootTable
end

local TISSDD = CreateFrame('Frame', 'TISSDD', UIParent, 'UIDropDownMenuTemplate')
local RightClickMenu

local function SpellLearnedFilter(self, event, message, ...)
	if strfind(message, gsub(ERR_LEARN_ABILITY_S, '%%s%.','.+%%.')) then
		return true
	elseif strfind(message, gsub(ERR_LEARN_SPELL_S, '%%s%.','.+%%.')) then
		return true
	elseif strfind(message, gsub(ERR_SPELL_UNLEARNED_S, '%%s%.','.+%%.')) then
		return true
	elseif strfind(message, gsub(ERR_LEARN_PASSIVE_S, '%%s%.','.+%%.')) then
		return true
	end
	
	return false, message, ...
end

local Update = function(self, event)
	RightClickMenu[2]['menuList'] = GetGearSets('Primary')
	RightClickMenu[3]['menuList'] = GetGearSets('Secondary')
	RightClickMenu[4]['menuList'] = GetGearSets('PvP')
	local inInstance, instanceType = IsInInstance()
	if not GetSpecialization() then
		self.Text:SetText(L.DataText.NoTalent)
	else
		local Tree = GetSpecialization()
		local Texture = format(L.DataText.TextureString, select(4, GetSpecializationInfo(Tree))) or 'N/A'
		local SameSpecLoot = false
		if GetLootSpecialization() == GetSpecializationInfo(Tree) or GetLootSpecialization() == 0 then
			SameSpecLoot = true
		end
		if SameSpecLoot then
			self.Text:SetFormattedText('%s%s%s', DataText.NameColor, L.DataText.SpecLoot, Texture)
		else
			local LootTexture = format(L.DataText.TextureString, select(4, GetSpecializationInfoByID(GetLootSpecialization()))) or 'N/A'
			self.Text:SetFormattedText('%s%s%s %s%s', DataText.NameColor, L.DataText.Spec, Texture, L.DataText.Loot, LootTexture)
		end
	end
	local PrintMissing = false
	local GearSet = GetActiveSpecGroup() == 1 and TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['Primary'] or TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['Secondary']
	local Icon, _, isEquipped, Total, Equipped, _, Missing = GetEquipmentSetInfoByName(GearSet)
	if not Icon then return end
	if isEquipped and Equipped == Total then return end
	if Missing > 0 then PrintMissing = true end
	if instanceType == 'party' or instanceType == 'raid' or event == 'ACTIVE_TALENT_GROUP_CHANGED' then
		UseEquipmentSet(GearSet)
	end
	if instanceType == 'pvp' or instanceType == 'arena' then
		UseEquipmentSet(TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['PvP'])
	end
	if PrintMissing then RaidNotice_AddMessage(RaidWarningFrame, format(EQUIPMENT_MANAGER_MISSING_ITEM, GearSet), ChatTypeInfo['RAID_WARNING']) end
end

local OnMouseDown = function(self, button)
	if button == 'LeftButton' then
		local Group = GetActiveSpecGroup(false, false)
		SetActiveSpecGroup(Group == 1 and 2 or 1)
	elseif button == 'RightButton' then
		GameTooltip:Hide()
		EasyMenu(RightClickMenu, TISSDD, 'cursor', 0, 0, 'MENU', 2)
	end
end

local function DurabilityColorGradient(perc, ...)
	if perc >= 1 then
		return select(select('#', ...) - 2, ...)
	elseif perc <= 0 then
		return ...
	end

	local num = select('#', ...) / 3
	local segment, relperc = math.modf(perc*(num-1))
	local r1, g1, b1, r2, g2, b2 = select((segment*3)+1, ...)

	return r1 + (r2-r1)*relperc, g1 + (g2-g1)*relperc, b1 + (b2-b1)*relperc
end

local function GetGearDurability(set)
	local EQSetItemIDs = GetEquipmentSetItemIDs(TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set])
	local EQSetItemLocations = GetEquipmentSetLocations(TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set])
	local isEquipped = select(3, GetEquipmentSetInfoByName(TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set]))
	local LeftString = format('%s (%s) - %s', L.DataText.EquipmentSet, set, TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set])
	if isEquipped then
		GameTooltip:AddDoubleLine(LeftString, L.DataText.Equipped, 1, 1, 1, 0, 1, 0)
		TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['ItemLevel'][set] = floor(select(2, GetAverageItemLevel()))
	else
		GameTooltip:AddLine(LeftString, 1, 1, 1)
	end
	if TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName][set] == 'None' then return end
	if TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['ItemLevel'][set] then GameTooltip:AddDoubleLine(STAT_AVERAGE_ITEM_LEVEL, TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['ItemLevel'][set], 1, 1, 1, .83, .55, 0) end
	sort(L.DataText.Slots, function(a, b) return a[1] < b[1] end)
	for k, v in pairs(EQSetItemIDs) do
		for i = 1, 10 do
			if L.DataText.Slots[i][1] == k then
				local player, bank, bags, voidstorage, slot, bag = EquipmentManager_UnpackLocation(tonumber(EQSetItemLocations[k]))
				if player and bags then
					local durability, maxdurability = GetContainerItemDurability(bag, slot)
					local r, g, b = DurabilityColorGradient((durability/maxdurability), 1, 0, 0, 1, 1, 0, 0, 1, 0)
					GameTooltip:AddDoubleLine(L.DataText.Slots[i][2]..' - '..select(2,GetItemInfo(v)), floor(durability/maxdurability * 100)..'%', 1, 1, 1, r, g, b)
				elseif player and not bags or v == 1 then
					local durability, maxdurability = GetInventoryItemDurability(k)
					local r, g, b = DurabilityColorGradient((durability/maxdurability), 1, 0, 0, 1, 1, 0, 0, 1, 0)
					GameTooltip:AddDoubleLine(L.DataText.Slots[i][2]..' - '..GetInventoryItemLink('player', k), floor(durability/maxdurability * 100)..'%', 1, 1, 1, r, g, b)
				elseif v ~= 0 then
					GameTooltip:AddDoubleLine(L.DataText.Slots[i][2]..' - '..select(2, GetItemInfo(v)), L.DataText.BankedMissingItem, 1, 1, 1, .77, .12, .23)
				end
			end
		end
	end
end

local OnEnter = function(self)
	if not InCombatLockdown() then
		local panel, anchor, xoff, yoff = self:GetTooltipAnchor()
		GameTooltip:SetOwner(panel, anchor, xoff, yoff)
		if GetNumEquipmentSets() > 0 then
			GameTooltip:ClearLines()
			GetGearDurability('Primary')
			GameTooltip:AddLine(' ')
			GetGearDurability('Secondary')
			GameTooltip:AddLine(' ')
			GetGearDurability('PvP')
		else
			GameTooltip:AddLine(L.DataText.NoEquipmentSets , 1, 1, 1)
		end
		GameTooltip:Show()
	end
end

local OnLeave = function(self)
	GameTooltip:Hide()
end

local Enable = function(self)	
	if not TukuiExtraDataTexts['SpecSwitch'] then TukuiExtraDataTexts['SpecSwitch'] = {} end
	if not TukuiExtraDataTexts['SpecSwitch'][MyRealm] then TukuiExtraDataTexts['SpecSwitch'][MyRealm] = {} end
	if not TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName] then TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName] = {} end
	if not TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['Primary'] then TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['Primary'] = 'None' end
	if not TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['Secondary'] then TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['Secondary'] = 'None' end
	if not TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['PvP'] then TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['PvP'] = 'None' end
	if not TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['ItemLevel'] then TukuiExtraDataTexts['SpecSwitch'][MyRealm][MyName]['ItemLevel'] = {} end

	RightClickMenu = {
		{ text = 'Improved Spec Switch Options', isTitle = true, notCheckable = true },
		{ text = 'Primary GearSet', notCheckable = true, hasArrow = true, menuList = GetGearSets('Primary') },
		{ text = 'Secondary GearSet', notCheckable = true, hasArrow = true, menuList = GetGearSets('Secondary') },
		{ text = 'PvP GearSet', notCheckable = true, hasArrow = true, menuList = GetGearSets('PvP') },
		{ text = SELECT_LOOT_SPECIALIZATION, notCheckable = true , hasArrow = true, menuList = LootSpecialization() },
	}

	self:RegisterEvent('PLAYER_ENTERING_WORLD')
	self:RegisterEvent('ACTIVE_TALENT_GROUP_CHANGED')
	self:RegisterEvent('CONFIRM_TALENT_WIPE')
	self:RegisterEvent('PLAYER_TALENT_UPDATE')
	self:RegisterEvent('PLAYER_LOOT_SPEC_UPDATED')
	self:RegisterEvent('EQUIPMENT_SETS_CHANGED')
	self:SetScript('OnEnter', OnEnter)
	self:SetScript('OnLeave', OnLeave)
	self:SetScript('OnEvent', Update)
	self:SetScript('OnMouseDown', OnMouseDown)
	ChatFrame_AddMessageEventFilter('CHAT_MSG_SYSTEM', SpellLearnedFilter)
	self:Update()
end

local Disable = function(self)
	self.Text:SetText('')
	self:UnregisterAllEvents()
	ChatFrame_RemoveMessageEventFilter('CHAT_MSG_SYSTEM', SpellLearnedFilter)
	self:SetScript('OnEvent', nil)
	self:SetScript('OnMouseDown', nil)
end

DataText:Register('SpecSwitch', Enable, Disable, Update)