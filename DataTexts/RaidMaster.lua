local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local format, gsub, pairs, select = format, gsub, pairs, select
local GetLootMethod, SetLootMethod, GetLootThreshold, SetLootThreshold, IsInInstance = GetLootMethod, SetLootMethod, GetLootThreshold, SetLootThreshold, IsInInstance
local GetSavedInstanceInfo, GetNumSavedInstances = GetSavedInstanceInfo, GetNumSavedInstances
local MyName = EDT.MyName
local ITEM_QUALITY_COLORS = ITEM_QUALITY_COLORS

local Update = function(self)
	local Method = GetLootMethod()
	local Threshold = GetLootThreshold()
	local MethodName = ''
	if Method == 'freeforall' then
		MethodName = LOOT_FREE_FOR_ALL
	elseif Method == 'group' then
		MethodName = LOOT_GROUP_LOOT
	elseif Method == 'master' then
		MethodName = LOOT_MASTER_LOOTER
	elseif Method == 'roundrobin' then
		MethodName = LOOT_ROUND_ROBIN
	elseif Method == 'needbeforegreed' then
		MethodName = LOOT_NEED_BEFORE_GREED
	end

	if select(2, IsInInstance()) == 'raid' and UnitIsGroupLeader('player') then
		if Method ~= 'master' then
			SetLootMethod('master', MyName)
		end
		if Threshold ~= 4 then SetLootThreshold(4) end
	end

	self.Text:SetFormattedText('%s%s', ITEM_QUALITY_COLORS[Threshold].hex, gsub(MethodName, format('%s: ', LOOT_NOUN), ''))
end

local function OnEnter(self)
	if InCombatLockdown() then return end
	local Panel, Anchor, xOff, yOff = self:GetTooltipAnchor()
	GameTooltip:SetOwner(Panel, Anchor, xOff, yOff)
	GameTooltip:ClearLines()
	GameTooltip:AddLine(CALENDAR_FILTER_RAID_LOCKOUTS, 1, 1, 1)
	GameTooltip:AddLine(' ')
	for i = 1, GetNumSavedInstances() do
		local Name, ID, Reset, Difficulty, Locked, Extended, _, isRaid, Players, DifficultyName, Boss, Defeated = GetSavedInstanceInfo(i)
		if isRaid and Locked then
			GameTooltip:AddDoubleLine(format('%s (%s)', Name, DifficultyName), format('%d / %d', Defeated, Boss), 1, 1, 1, 1, 1, 1)
		end
	end
	GameTooltip:Show()
end

local Enable = function(self)
	self:RegisterEvent('UPDATE_INSTANCE_INFO')
	self:RegisterEvent('PLAYER_ENTERING_WORLD')
	self:RegisterEvent('PARTY_LEADER_CHANGED')
	self:RegisterEvent('GROUP_ROSTER_UPDATE')
	self:RegisterEvent('PARTY_LOOT_METHOD_CHANGED')
	self:SetScript('OnEnter', OnEnter)
	self:SetScript('OnLeave', GameTooltip_Hide)
	self:SetScript('OnEvent', Update)
	self:Update()
end

local Disable = function(self)
	self.Text:SetText('')
	self:SetScript('OnEvent', nil)
	self:UnregisterAllEvents()
end

DataText:Register('RaidMaster', Enable, Disable, Update)