local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local format, floor = format, floor
local GetPlayerMapPosition = GetPlayerMapPosition

local Update = function(self, event, ...)
	local Zone = GetZonePVPInfo()
	local R, G, B = 0, 1, 0
	if Zone == 'arena' or Zone == 'combat' or Zone == 'hostile' then
		R, G, B = 1, 0, 0
	elseif Zone == 'contested' then
		R, G, B = 1, 1, 0
	elseif Zone == 'sanctuary' then
		R, G, B = 0, .9, .9
	end
	local PosX, PosY = GetPlayerMapPosition('player')
	self.Text:SetFormattedText('|cFFFFFF00%s|r %s%s|r |cFFFFFF00%s|r', floor(100 * PosX), EDT.RGBToHex(R, G, B), GetMinimapZoneText(), floor(100 * PosY))
end

local Enable = function(self)
	self:SetScript('OnUpdate', Update)
end

local Disable = function(self)
	self.Text:SetText('')
	self:SetScript('OnEvent', nil)
	self:UnregisterAllEvents()
end

DataText:Register('Location', Enable, Disable, Update)