local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local format, pairs, select, tinsert, abs, mod = format, pairs, select, tinsert, abs, mod
local HasNewMail, TakeInboxMoney, GetInboxHeaderInfo, GetInboxItem = HasNewMail, TakeInboxMoney, GetInboxHeaderInfo, GetInboxItem
local NewMail, ReadMail, UnreadMail, ItemTable = false, {}, {}, {}

local Update = function(self, event, ...)
	if event == 'UPDATE_PENDING_MAIL' or event == 'PLAYER_ENTERING_WORLD' then
		NewMail = HasNewMail()
	end
	if event == 'MAIL_INBOX_UPDATE' or event == 'MAIL_SHOW' or event == 'MAIL_CLOSED' then
		wipe(ReadMail)
		wipe(UnreadMail)
		wipe(ItemTable)
		for i = 1, GetInboxNumItems() do
			TakeInboxMoney(i)
			local _, _, Sender, _, Money, COD, _, ItemCount, Read = GetInboxHeaderInfo(i)
			if ItemCount ~= nil then
				for j = 1, ItemCount do
					local ItemName, ItemTexture, Amount, Quality = GetInboxItem(i, j)
					if ItemName ~= nil then
						Quality = ITEM_QUALITY_COLORS[Quality].hex
						tinsert(ItemTable, { ['ItemName'] = ItemName, ['ItemTexture'] = ItemTexture, ['ItemAmount'] = Amount, ['Quality'] = Quality })
					end
				end
			end
			if Read then
				tinsert(ReadMail, { ['Sender'] = Sender, ['Money'] = Money, ['COD'] = COD, ['ItemCount'] = ItemCount, ['ItemTable'] = ItemTable } )
			else
				tinsert(UnreadMail, { ['Sender'] = Sender, ['Money'] = Money, ['COD'] = COD, ['ItemCount'] = ItemCount, ['ItemTable'] = ItemTable } )
				NewMail = true
			end
		end
	end

	if NewMail then
		self.Text:SetFormattedText('%s %s', NEW, MAIL_LABEL)
	else
		self.Text:SetFormattedText('%s %s', NO, MAIL_LABEL)
	end
end

local FormatMoney = function(money)
	local Gold = floor(abs(money) / 10000)
	local Silver = mod(floor(abs(money) / 100), 100)
	local Copper = mod(floor(abs(money)), 100)

	if (Gold ~= 0) then
		return format('%s%s%s %s%d%s %s%d%s', DataText.NameColor, EDT.Comma(Gold), L.DataText.GoldShort, DataText.NameColor, Silver, L.DataText.SilverShort, DataText.NameColor, Copper, L.DataText.CopperShort)
	elseif (Silver ~= 0) then
		return format('%s%d%s %s%d%s', DataText.NameColor, Silver, L.DataText.SilverShort, DataText.NameColor, Copper, L.DataText.CopperShort)
	else
		return format('%s%d%s', DataText.NameColor, Copper, L.DataText.CopperShort)
	end
end

local FormatMailTooltip = function(Table)
	for Key, Value in pairs(Table) do
		local r, g, b = 1, 1, 0
		local MoneyString = L.Tooltips.NoMoney
		if Value.Money > 0 then
			r, g, b = 0, 1, 0
			MoneyString = FormatMoney(Value.Money)
		elseif Value.COD > 0 then
			r, g, b = 1, 0, 0
			MoneyString = FormatMoney(Value.COD)
		end
		if Key ~= 1 then GameTooltip:AddLine(' ') end
		GameTooltip:AddDoubleLine(Value.Sender, MoneyString, r, g, b, 1, 1, 1)
		for i = 1, #Value.ItemTable do
			GameTooltip:AddDoubleLine(' ', format('%s%s (%d)', Value.ItemTable[i].Quality, Value.ItemTable[i].ItemName, Value.ItemTable[i].ItemAmount))
		end
	end
end

local OnEnter = function(self)
	if InCombatLockdown() then return end
	local Panel, Anchor, xOff, yOff = self:GetTooltipAnchor()
	GameTooltip:SetOwner(Panel, Anchor, xOff, yOff)
	GameTooltip:ClearLines()
	if #UnreadMail > 0 then
		GameTooltip:AddLine(L.DataText.UnreadMail, 1, 1, 1)
		GameTooltip:AddLine(' ')
		FormatMailTooltip(UnreadMail)
		if #ReadMail > 0 then GameTooltip:AddLine(' ') end
	end
	if #ReadMail > 0 then
		GameTooltip:AddLine(L.DataText.ReadMail, 1, 1, 1)
		GameTooltip:AddLine(' ')
		FormatMailTooltip(ReadMail)
	end
	GameTooltip:Show()
end

local Enable = function(self)
	self:RegisterEvent('MAIL_INBOX_UPDATE')
	self:RegisterEvent('PLAYER_ENTERING_WORLD')
	self:RegisterEvent('UPDATE_PENDING_MAIL')
	self:RegisterEvent('MAIL_CLOSED')
	self:RegisterEvent('MAIL_SHOW')
	self:SetScript('OnEnter', OnEnter)
	self:SetScript('OnLeave', GameTooltip_Hide)
	self:SetScript('OnEvent', Update)
	self:Update()
end

local Disable = function(self)
	self.Text:SetText('')
	self:SetScript('OnEvent', nil)
	self:UnregisterAllEvents()
end

DataText:Register('Mail', Enable, Disable, Update)