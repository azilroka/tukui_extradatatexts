local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local format, floor, abs, mod, pairs = format, floor, abs, mod, pairs
local GetMoney, GetCurrencyInfo, GetNumWatchedTokens, GetBackpackCurrencyInfo, UnitLevel = GetMoney, GetCurrencyInfo, GetNumWatchedTokens, GetBackpackCurrencyInfo, UnitLevel

TukuiExtraDataTexts['Currency'] = {
	['Archaeology'] = true,
	['Cooking'] = true,
	['Details'] = true,
	['Weekly'] = true,
	['Faction'] = true,
	['Icons'] = true,
	['Jewelcrafting'] = true,
	['Miscellaneous'] = true,
	['PvP'] = true,
	['Raid'] = true,
	['Zero'] = true,
}

local Profit, Spent, OldMoney = 0, 0, 0
local MyRealm = EDT.MyRealm
local MyName = EDT.MyName
local MyClass = EDT.MyClass
local JEWELCRAFTING, COOKING, ARCHAEOLOGY
local MyFaction = UnitFactionGroup('player')
local HordeColor = RAID_CLASS_COLORS['DEATHKNIGHT']
local AllianceColor = RAID_CLASS_COLORS['SHAMAN']

local ArchaeologyFragments = {
	398, -- Draenai
	384, -- Dwarf
	393, -- Fossil
	677, -- Mogu
	400, -- Nerubian
	394, -- Night Elf
	828, -- Ogre
	397, -- Orc
	676, -- Pandaren
	401, -- Tol'vir
	385, -- Troll
	399, -- Vrykul
	754, -- Mantid
	829, -- Arakkoa
	821, -- Draenai Clan
}

local CookingAwards = {
	81, -- Epicurean
	402 -- Ironpaw
}

local JewelcraftingTokens = {
	61, -- Dalaran
	361, -- Illustrious
}

local DungeonRaid = {
	776, -- Warforged Seal
	752, -- Mogu Rune of Fate
	697, -- Elder Charm
	738, -- Lesser Charm
	615, -- Essence of Corrupted Deathwing
	614, -- Mote of Darkness
	994, -- Seal of Tempered Fate
	1129,	-- Seal of Inevitable Fate
	1166,	-- Timewarped Badge
}

local PvPPoints = {
	390, -- Conquest
	392, -- Honor
	391, -- Tol Barad
}

local MiscellaneousCurrency = {
	241, -- Champion Seals
	416, -- Mark of the World Tree
	515, -- Darkmoon Prize Ticket
	777, -- Timeless Coins
	944, -- Artifact Fragment?
	789, -- Bloody Coin
	823, -- Apexis Crystal
	980, -- Dingy Iron Coins
	824, -- Garrison
	1101, -- Oil
}

local function GetOption(name)
	return TukuiExtraDataTexts['Currency'][name]
end

local function SetOption(name, value)
	TukuiExtraDataTexts['Currency'][name] = value
end

local function ToggleOption(name)
	if TukuiExtraDataTexts['Currency'][name] then
		TukuiExtraDataTexts['Currency'][name] = false
	else
		TukuiExtraDataTexts['Currency'][name] = true
	end
end

local FormatMoney = function(money)
	local Gold = floor(abs(money) / 10000)
	local Silver = mod(floor(abs(money) / 100), 100)
	local Copper = mod(floor(abs(money)), 100)

	if (Gold ~= 0) then
		return format('%s%s%s %s%d%s %s%d%s', DataText.NameColor, EDT.Comma(Gold), L.DataText.GoldShort, DataText.NameColor, Silver, L.DataText.SilverShort, DataText.NameColor, Copper, L.DataText.CopperShort)
	elseif (Silver ~= 0) then
		return format('%s%d%s %s%d%s', DataText.NameColor, Silver, L.DataText.SilverShort, DataText.NameColor, Copper, L.DataText.CopperShort)
	else
		return format('%s%d%s', DataText.NameColor, Copper, L.DataText.CopperShort)
	end
end

local function GetCurrency(CurrencyTable, Text)
	local Seperator = false
	for key, id in pairs(CurrencyTable) do
		local name, amount, texture, week, weekmax, maxed, discovered = GetCurrencyInfo(id)
		local LeftString = GetOption('Icons') and format('%s %s', format(L.Tooltips.TextureString, texture), name) or name
		local RightString = amount
		if id == 392 then
			maxed = tonumber(strsub(tostring(maxed), 0, 4))
			weekmax = tonumber(strsub(tostring(weekmax), 0, 4))
		end

		if id == 390 then
			discovered = UnitLevel('player') >= SHOW_CONQUEST_LEVEL
		elseif maxed > 0 then
			RightString = format('%s / %s', amount, maxed)
		end

		if weekmax > 1 then
			if id == 390 then
				if GetOption('Details') then RightString = format('%s %s | %s %s / %s', 'Current:', amount, 'Weekly:', week, weekmax) end
			else
				if GetOption('Details') then RightString = format('%s %s / %s | %s %s / %s', 'Current:', amount, maxed, 'Weekly:', week, weekmax) end
			end
			if GetOption('Weekly') then RightString = format('%s / %s', week, weekmax) end
		end

		local r1, g1, b1 = 1, 1, 1
		for i = 1, GetNumWatchedTokens() do
			local _, _, _, itemID = GetBackpackCurrencyInfo(i)
			if id == itemID then
				r1, g1, b1 = .24, .54, .78
			end
		end
		local r2, g2, b2 = r1, g1, b1
		if maxed > 0 and (amount == maxed) or weekmax > 0 and (week == weekmax) then r2, g2, b2 = .77, .12, .23 end
		if not (amount == 0 and not GetOption('Zero') and r1 == 1) and discovered then
			if not Seperator then
				GameTooltip:AddLine(' ')
				GameTooltip:AddLine(Text)
				Seperator = true
			end
			GameTooltip:AddDoubleLine(LeftString, RightString, r1, g1, b1, r2, g2, b2)
		end
	end
end

local function ResetGold()
	TukuiExtraDataTexts['Gold'][MyRealm] = {}
	TukuiExtraDataTexts['Gold'][MyRealm][MyName] = {}
	TukuiExtraDataTexts['Gold'][MyRealm][MyName]['Gold'] = GetMoney()
	TukuiExtraDataTexts['Gold'][MyRealm][MyName]['Class'] = MyClass
	EDT:Print(L.DataText.GoldReset)
end

local RightClickMenu = {
	{ text = 'Tukui Improved Currency Options', isTitle = true , notCheckable = true },
	{ text = 'Show Archaeology Fragments', checked = function() return GetOption('Archaeology') end, func = function() ToggleOption('Archaeology') end },
	{ text = 'Show Jewelcrafting Tokens', checked = function()  return GetOption('Jewelcrafting') end, func = function() ToggleOption('Jewelcrafting') end },
	{ text = 'Show Player vs Player Currency', checked = function() return GetOption('PvP') end, func = function() ToggleOption('PvP') end },
	{ text = 'Show Dungeon and Raid Currency', checked = function() return GetOption('Raid') end, func = function() ToggleOption('Raid') end },
	{ text = 'Show Cooking Awards', checked = function() return GetOption('Cooking') end, func = function() ToggleOption('Cooking') end },
	{ text = 'Show Miscellaneous Currency', checked = function() return GetOption('Miscellaneous') end, func = function() ToggleOption('Miscellaneous') end },
	{ text = 'Show Zero Currency', checked = function() return GetOption('Zero') end, func = function() ToggleOption('Zero') end },
	{ text = 'Show Weekly Totals', checked = function() return GetOption('Weekly') end, func = function() ToggleOption('Weekly') SetOption('Details', false) end },
	{ text = 'Show Weekly & Max Totals', checked = function() return GetOption('Details') end, func = function() ToggleOption('Details') SetOption('Weekly', false) end },
	{ text = 'Show Icons', checked = function() return GetOption('Icons') end, func = function() ToggleOption('Icons') end },
	{ text = 'Show Faction Totals', checked = function() return GetOption('Faction') end, func = function() ToggleOption('Faction') end },
}

local TukuiImprovedCurrencyDropDown = CreateFrame('Frame', 'TukuiImprovedCurrencyDropDown', UIParent, 'UIDropDownMenuTemplate')

local OnEnter = function(self)
	if not InCombatLockdown() then
		local Panel, Anchor, xOff, yOff = self:GetTooltipAnchor()

		GameTooltip:SetOwner(Panel, Anchor, xOff, yOff)
		GameTooltip:ClearLines()
		GameTooltip:AddLine(L.DataText.Session)
		GameTooltip:AddDoubleLine(L.DataText.Earned, FormatMoney(Profit), 1, 1, 1, 1, 1, 1)
		GameTooltip:AddDoubleLine(L.DataText.Spent, FormatMoney(Spent), 1, 1, 1, 1, 1, 1)

		if (Profit < Spent) then
			GameTooltip:AddDoubleLine(L.DataText.Deficit, FormatMoney(Profit-Spent), 1, 0, 0, 1, 1, 1)
		elseif ((Profit-Spent) > 0) then
			GameTooltip:AddDoubleLine(L.DataText.Profit, FormatMoney(Profit-Spent), 0, 1, 0, 1, 1, 1)
		end

		GameTooltip:AddLine(' ')

		local TotalGold, HordeGold, AllianceGold = 0, 0, 0
		GameTooltip:AddLine(L.DataText.Character)
		for Player, Info in EDT:OrderedPairs(TukuiExtraDataTexts['Gold'][MyRealm]) do
			local Color = RAID_CLASS_COLORS[Info['Class']]
			GameTooltip:AddDoubleLine(Player, FormatMoney(Info['Gold']), Color.r, Color.g, Color.b, 1, 1, 1)
			if TukuiExtraDataTexts['Faction'][MyRealm]['Alliance'][Player] then
				AllianceGold = AllianceGold + Info['Gold']
			end
			if TukuiExtraDataTexts['Faction'][MyRealm]['Horde'][Player] then
				HordeGold = HordeGold + Info['Gold']
			end
			TotalGold = TotalGold + Info['Gold']
		end

		GameTooltip:AddLine(' ')
		GameTooltip:AddLine(L.DataText.Server)
		if GetOption('Faction') then
			GameTooltip:AddDoubleLine(format('%s: ', FACTION_HORDE), FormatMoney(HordeGold), HordeColor.r, HordeColor.g, HordeColor.b, 1, 1, 1)
			GameTooltip:AddDoubleLine(format('%s: ', FACTION_ALLIANCE), FormatMoney(AllianceGold), AllianceColor.r, AllianceColor.g, AllianceColor.b, 1, 1, 1)
		end
		GameTooltip:AddDoubleLine(L.DataText.Total, FormatMoney(TotalGold), 1, 1, 1, 1, 1, 1)

		if ARCHAEOLOGY ~= nil and GetOption('Archaeology') then
			GetCurrency(ArchaeologyFragments, format('%s %s:', ARCHAEOLOGY, ARCHAEOLOGY_RUNE_STONES))
		end
		if COOKING ~= nil and GetOption('Cooking') then
			GetCurrency(CookingAwards, format("%s:", COOKING))
		end
		if JEWELCRAFTING ~= nil and GetOption('Jewelcrafting') then
			GetCurrency(JewelcraftingTokens, format("%s:", JEWELCRAFTING))
		end
		if GetOption('Raid') then
			GetCurrency(DungeonRaid, format('%s & %s:', CALENDAR_TYPE_DUNGEON, CALENDAR_TYPE_RAID))
		end
		if GetOption('PvP') then
			GetCurrency(PvPPoints, format("%s:", PLAYER_V_PLAYER))
		end
		if GetOption('Miscellaneous') then
			GetCurrency(MiscellaneousCurrency, format("%s:", MISCELLANEOUS))
		end
		GameTooltip:AddLine(' ')
		GameTooltip:AddLine(L.DataText.ResetInfo)
		GameTooltip:Show()
	end
end

local OnLeave = function(self)
	GameTooltip:Hide()
end

local Update = function(self, event)
	if (not IsLoggedIn()) then
		return
	end

	JEWELCRAFTING = nil
	for k, v in pairs({GetProfessions()}) do
		if v then
			local name, _, _, _, _, _, skillid = GetProfessionInfo(v)
			if skillid == 755 then
				JEWELCRAFTING = name
			elseif skillid == 185 then
				COOKING = name
			elseif skillid == 794 then
				ARCHAEOLOGY = name
			end
		end
	end

	local NewMoney = GetMoney()

	TukuiExtraDataTexts['Gold'] = TukuiExtraDataTexts['Gold'] or {}
	TukuiExtraDataTexts['Gold'][MyRealm] = TukuiExtraDataTexts['Gold'][MyRealm] or {}
	TukuiExtraDataTexts['Gold'][MyRealm][MyName] = TukuiExtraDataTexts['Gold'][MyRealm][MyName] or {}
	TukuiExtraDataTexts['Gold'][MyRealm][MyName]['Class'] = MyClass
	TukuiExtraDataTexts['Faction'] = TukuiExtraDataTexts['Faction'] or {}
	TukuiExtraDataTexts['Faction'][MyRealm] = TukuiExtraDataTexts['Faction'][MyRealm] or {}
	TukuiExtraDataTexts['Faction'][MyRealm][FACTION_HORDE] = TukuiExtraDataTexts['Faction'][MyRealm][FACTION_HORDE] or {}
	TukuiExtraDataTexts['Faction'][MyRealm][FACTION_ALLIANCE] = TukuiExtraDataTexts['Faction'][MyRealm][FACTION_ALLIANCE] or {}
	local OldMoney = TukuiExtraDataTexts['Gold'][MyRealm][MyName]['Gold'] or NewMoney

	local Change = NewMoney - OldMoney
	if (OldMoney > NewMoney) then
		Spent = Spent - Change
	else
		Profit = Profit + Change
	end

	self.Text:SetText(FormatMoney(NewMoney))

	TukuiExtraDataTexts['Gold'][MyRealm][MyName]['Gold'] = NewMoney
	TukuiExtraDataTexts['Faction'][MyRealm][MyFaction][MyName] = NewMoney
end

local OnMouseDown = function(self, button)
	GameTooltip:Hide()
	if button == 'LeftButton' then
		ToggleAllBags()
	elseif button == 'RightButton' then
		if IsShiftKeyDown()then
			ResetGold()
		else
			EasyMenu(RightClickMenu, TukuiImprovedCurrencyDropDown, 'cursor', 0, 0, 'MENU', 2)
		end
	end
end

local Enable = function(self)
	self:RegisterEvent("PLAYER_MONEY")
	self:RegisterEvent("SEND_MAIL_MONEY_CHANGED")
	self:RegisterEvent("SEND_MAIL_COD_CHANGED")
	self:RegisterEvent("PLAYER_TRADE_MONEY")
	self:RegisterEvent("TRADE_MONEY_CHANGED")
	self:RegisterEvent("PLAYER_ENTERING_WORLD")
	self:SetScript('OnEvent', Update)
	self:SetScript('OnMouseDown', OnMouseDown)
	self:SetScript('OnEnter', OnEnter)
	self:SetScript('OnLeave', OnLeave)
	self:Update()
end

local Disable = function(self)
	self.Text:SetText('')
	self:UnregisterAllEvents()
	self:SetScript('OnEvent', nil)
	self:SetScript('OnMouseDown', nil)
	self:SetScript('OnEnter', nil)
	self:SetScript('OnLeave', nil)
end

DataText:Register('Gold & Currency', Enable, Disable, Update)