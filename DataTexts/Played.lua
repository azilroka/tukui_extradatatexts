local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local format, pairs, GetTime, ChatFrame_TimeBreakDown, InCombatLockdown = format, pairs, GetTime, ChatFrame_TimeBreakDown, InCombatLockdown
local PlayedTimeFormatFull = '%d D %02d:%02d:%02d'
local PlayedTimeFormatNoDay = '%02d:%02d:%02d'
local TotalPlayTime, LevelPlayTime, SessionPlayTime, LevelPlayedOffset, LastLevelTime
local MyRealm = EDT.MyRealm
local MyName = EDT.MyName
local MyClass = EDT.MyClass

local OnEnter = function(self)
	if not InCombatLockdown() and SessionPlayTime then
		local SessionDay, SessionHour, SessionMinute, SessionSecond = ChatFrame_TimeBreakDown(GetTime() - SessionPlayTime)
		local TotalDay, TotalHour, TotalMinute, TotalSecond = ChatFrame_TimeBreakDown(TotalPlayTime + (GetTime() - SessionPlayTime))
		local LevelDay, LevelHour, LevelMinute, LevelSecond = ChatFrame_TimeBreakDown(LevelPlayTime + (GetTime() - LevelPlayTimeOffset))
		local LastLevelDay, LastLevelHour, LastLevelMinute, LastLevelSecond = ChatFrame_TimeBreakDown(LastLevelTime)
		local Panel, Anchor, xOff, yOff = self:GetTooltipAnchor()
		GameTooltip:SetOwner(Panel, Anchor, xOff, yOff)
		GameTooltip:ClearLines()
		GameTooltip:AddLine(TIME_PLAYED_MSG, 1, 1, 1)
		GameTooltip:AddLine(' ')
		GameTooltip:AddDoubleLine(L.DataText.Session, SessionDay > 0 and format(PlayedTimeFormatFull, SessionDay, SessionHour, SessionMinute, SessionSecond) or format(PlayedTimeFormatNoDay, SessionHour, SessionMinute, SessionSecond), 1, 1, 1, 1, 1, 1)
		if LastLevelSecond > 0 then
			GameTooltip:AddDoubleLine(L.DataText.PreviousLevel, LastLevelDay > 0 and format(PlayedTimeFormatFull, LastLevelDay, LastLevelHour, LastLevelMinute, LastLevelSecond) or format(PlayedTimeFormatNoDay, LastLevelHour, LastLevelMinute, LastLevelSecond), 1, 1, 1, 1, 1, 1)
		end
		GameTooltip:AddDoubleLine(format('%s:', LEVEL), LevelDay > 0 and format(PlayedTimeFormatFull, LevelDay, LevelHour, LevelMinute, LevelSecond) or format(PlayedTimeFormatNoDay, LevelHour, LevelMinute, LevelSecond), 1, 1, 1, 1, 1, 1)
		GameTooltip:AddDoubleLine(L.DataText.Total, TotalDay > 0 and format(PlayedTimeFormatFull, TotalDay, TotalHour, TotalMinute, TotalSecond) or format(PlayedTimeFormatNoDay, TotalHour, TotalMinute, TotalSecond), 1, 1, 1, 1, 1, 1)
		GameTooltip:AddLine(' ')
		GameTooltip:AddLine(L.DataText.RealmTimePlayed, 1, 1, 1)
		GameTooltip:AddLine(' ')
		local Class, Level, AccountDay, AccountHour, AccountMinute, AccountSecond, TotalAccountTime
		for player, subtable in EDT:OrderedPairs(TukuiExtraDataTexts['TimePlayed'][MyRealm]) do
			for k, v in pairs(subtable) do
				if k == 'TotalTime' then
					AccountDay, AccountHour, AccountMinute, AccountSecond = ChatFrame_TimeBreakDown(v)
					TotalAccountTime = (TotalAccountTime or 0) + v
				end
				if k == 'Class' then Class = v end
				if k == 'Level' then Level = v end
			end
			local color = RAID_CLASS_COLORS[Class]
			GameTooltip:AddDoubleLine(format('%s |cFFFFFFFF- %s %d', player, LEVEL, Level), format(PlayedTimeFormatFull, AccountDay, AccountHour, AccountMinute, AccountSecond), color.r, color.g, color.b, 1, 1, 1)
		end
		GameTooltip:AddLine(' ')
		local TotalAccountDay, TotalAccountHour, TotalAccountMinute, TotalAccountSecond = ChatFrame_TimeBreakDown(TotalAccountTime)
		GameTooltip:AddDoubleLine(L.DataText.Total, format(PlayedTimeFormatFull, TotalAccountDay, TotalAccountHour, TotalAccountMinute, TotalAccountSecond), 1, 0, 1, 1, 1, 1)
		GameTooltip:AddLine(' ')
		GameTooltip:AddLine(L.DataText.ResetInfo)
		GameTooltip:Show()
	end
end

local ElapsedTimer = 0
local OnUpdate = function(self, elapsed)
	ElapsedTimer = ElapsedTimer + elapsed
	if TotalPlayTime and LevelPlayTime and SessionPlayTime then
		local Day, Hour, Minute, Second
		if UnitLevel('player') ~= MAX_PLAYER_LEVEL then
			Day, Hour, Minute, Second = ChatFrame_TimeBreakDown(LevelPlayTime + (GetTime() - LevelPlayTimeOffset))
		else
			Day, Hour, Minute, Second = ChatFrame_TimeBreakDown(TotalPlayTime + (GetTime() - SessionPlayTime))
		end
		if Day > 0 then
			self.Text:SetFormattedText('%d D %02d:%02d', Day, Hour, Minute)
		else
			self.Text:SetFormattedText('%02d:%02d', Hour, Minute)
		end
	else
		if ElapsedTimer > 1 and not self.Requested then self.Requested = true RequestTimePlayed() end
	end
end

local OnEvent = function(self, event, ...)
	if not TukuiExtraDataTexts['TimePlayed'] then TukuiExtraDataTexts['TimePlayed'] = {} end
	if not TukuiExtraDataTexts['TimePlayed'][MyRealm] then TukuiExtraDataTexts['TimePlayed'][MyRealm] = {} end
	if not TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName] then TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName] = {} end
	TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['Class'] = MyClass
	TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['Level'] = UnitLevel('player')
	LastLevelTime = TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['LastLevelTime'] or 0
	if event == 'TIME_PLAYED_MSG' then
		local TotalTime, LevelTime = ...
		TotalPlayTime = TotalTime
		LevelPlayTime = LevelTime
		if SessionPlayTime == nil then SessionPlayTime = GetTime() end
		LevelPlayTimeOffset = GetTime()
		TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['TotalTime'] = TotalTime
		TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['LevelTime'] = LevelTime
	end
	if event == 'PLAYER_LEVEL_UP' then
		LastLevelTime = floor(LevelPlayTime + (GetTime() - LevelPlayTimeOffset))
		TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['LastLevelTime'] = LastLevelTime
		LevelPlayTime = 1
		LevelPlayTimeOffset = GetTime()
		TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['Level'] = UnitLevel('player')
	end
	if event == 'PLAYER_LOGOUT' then
		RequestTimePlayed()
	end
end

local function Reset()
	TukuiExtraDataTexts['TimePlayed'][MyRealm] = {}
	TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName] = {}
	TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['Level'] = UnitLevel('player')
	TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['LastLevelTime'] = LastLevelTime
	TukuiExtraDataTexts['TimePlayed'][MyRealm][MyName]['Class'] = MyClass
	RequestTimePlayed()
	EDT:Print(L.DataText.TimeReset)
end

local OnMouseDown = function(self, button)
	if button == 'RightButton' then
		if IsShiftKeyDown()then
			Reset()
		end
	end
end

local Enable = function(self)	
	self.Text:SetText(TIME_PLAYED_MSG)
	self:RegisterEvent('TIME_PLAYED_MSG')
	self:RegisterEvent('PLAYER_LEVEL_UP')
	self:RegisterEvent('PLAYER_ENTERING_WORLD')
	self:RegisterEvent('PLAYER_LOGOUT')
	self:SetScript('OnMouseDown', OnMouseDown)
	self:SetScript('OnUpdate', OnUpdate)
	self:SetScript('OnEnter', OnEnter)
	self:SetScript('OnEvent', OnEvent)
	self:SetScript('OnLeave', GameTooltip_Hide)
	self:Update(1)
end

local Disable = function(self)
	self.Text:SetText('')
	self:UnregisterAllEvents()
	self:SetScript('OnEvent', nil)
	self:SetScript('OnEnter', nil)
	self:SetScript('OnLeave', nil)
end

DataText:Register('Time Played', Enable, Disable, OnEvent)