local EDT = unpack(select(2, ...))
local L = EDT.L
local DataText = EDT['DataTexts']

local format, sort, pairs, GetProfessions, GetProfessionInfo = format, sort, pairs, GetProfessions, GetProfessionInfo
local MyRealm = EDT.MyRealm
local MyName = EDT.MyName
local MyClass = EDT.MyClass

local Profession1Name, Profession1Texture, Profession1Rank, Profession1Max
local Profession2Name, Profession2Texture, Profession2Rank, Profession2Max
local Panel, Anchor, xOff, yOff

local OnEnter = function(self)
	if not InCombatLockdown() then
		GameTooltip:SetOwner(Panel, Anchor, xOff, yOff)
		GameTooltip:ClearLines()
		for player, subtable in EDT:OrderedPairs(TukuiExtraDataTexts['Professions'][MyRealm]) do
			local Class, P1N, P1T, P1R, P1M, P2N, P2T, P2R, P2M
			for key, value in pairs(subtable) do
				if key == 'Class' then Class = value end
				if key == 'Profession1Name' then P1N = value end
				if key == 'Profession2Name' then P2N = value end
				if key == 'Profession1Rank' then P1R = value end
				if key == 'Profession2Rank' then P2R = value end
				if key == 'Profession1Max' then P1M = value end
				if key == 'Profession2Max' then P2M = value end
				if key == 'Profession1Texture' then P1T = value end
				if key == 'Profession2Texture' then P2T = value end
			end
			local color = RAID_CLASS_COLORS[Class]
			GameTooltip:AddLine(player, color.r, color.g, color.b)
			if P1N ~= nil and P1N ~= "N/A" then
				GameTooltip:AddDoubleLine(format('%s %s', format(L.Tooltips.TextureString, P1T), P1N), format('%s / %s', P1R, P1M), 1, 1, 1, 1, 1, 1)
			else
				GameTooltip:AddLine(L.DataText.NoProfession)
			end
			if P2N ~= nil and P2N ~= "N/A" then
				GameTooltip:AddDoubleLine(format('%s %s', format(L.Tooltips.TextureString, P2T), P2N), format('%s / %s', P2R, P2M), 1, 1, 1, 1, 1, 1)
			else
				GameTooltip:AddLine(L.DataText.NoProfession)
			end
			GameTooltip:AddLine(' ')
		end
		GameTooltip:AddLine(L.DataText.ResetInfo)
		GameTooltip:Show()
	end
end

local function Reset()
	TukuiExtraDataTexts['Professions'][MyRealm] = {}
	TukuiExtraDataTexts['Professions'][MyRealm][MyName] = {}
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Class'] = MyClass
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Name'] = Profession1Name or "N/A"
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Name'] = Profession2Name or "N/A"
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Rank'] = Profession1Rank or 0
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Rank'] = Profession2Rank or 0
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Max'] = Profession1Max or 0
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Max'] = Profession2Max or 0
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Texture'] = Profession1Texture or "N/A"
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Texture'] = Profession2Texture or "N/A"
	EDT:Print(L.DataText.ProfessionsReset)
end

local OnMouseDown = function(self, button)
	if button == 'LeftButton' then
		CastSpellByName(Profession1Name)
	elseif button == 'RightButton' then
		if IsShiftKeyDown()then
			Reset()
		else
			CastSpellByName(Profession2Name)
		end
	end
end

local OnEvent = function(self, event, ...)
	local Profession1Index, Profession2Index = GetProfessions()
	local DTProfession1, DTProfession2 = '', ''
	local Info = L.DataText.NoProfession
	if Profession1Index ~= nil then 
		Profession1Name, Profession1Texture, Profession1Rank, Profession1Max = GetProfessionInfo(Profession1Index)
		DTProfession1 = format('%s %d', format(L.DataText.TextureString, Profession1Texture), Profession1Rank)
		Info = DTProfession1
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Name'] = Profession1Name
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Rank'] = Profession1Rank
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Max'] = Profession1Max
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession1Texture'] = Profession1Texture
	end
	if Profession2Index ~= nil then
		Profession2Name, Profession2Texture, Profession2Rank, Profession2Max = GetProfessionInfo(Profession2Index)
		DTProfession2 = format('%s %d', format(L.DataText.TextureString, Profession2Texture), Profession2Rank)
		Info = DTProfession2
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Name'] = Profession2Name
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Rank'] = Profession2Rank
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Max'] = Profession2Max
		TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Profession2Texture'] = Profession2Texture
	end
	if Profession1Index and Profession2Index then
		Info = format('%s %s', DTProfession1, DTProfession2)
	end
	self.Text:SetText(Info)
	if not InCombatLockdown() then
		if not self.SecureButton:IsShown() then
			self.SecureButton:Show()
		end
		self.SecureButton:SetSize(self.Text:GetSize())
	end
end

local OnUpdate = function(self, elapsed)
	if self.Position then
		Panel, Anchor, xOff, yOff = self:GetTooltipAnchor()
		self.SecureButton:SetPoint("CENTER", DataText.Anchors[self.Position], 'CENTER', 0, 0)
		self:SetScript('OnUpdate', nil)
	end
end

local Enable = function(self)
	if not self.SecureButton then
		local SecureButton = CreateFrame('Button', nil, self, 'SecureActionButtonTemplate')
		SecureButton:RegisterForClicks("AnyDown")
		SecureButton:SetScript("OnClick", OnMouseDown)
		SecureButton:SetScript('OnEnter', OnEnter)
		SecureButton:SetScript('OnLeave', GameTooltip_Hide)

		self.SecureButton = SecureButton
	end

	if not TukuiExtraDataTexts['Professions'] then TukuiExtraDataTexts['Professions'] = {} end
	if not TukuiExtraDataTexts['Professions'][MyRealm] then TukuiExtraDataTexts['Professions'][MyRealm] = {} end
	if not TukuiExtraDataTexts['Professions'][MyRealm][MyName] then TukuiExtraDataTexts['Professions'][MyRealm][MyName] = {} end
	TukuiExtraDataTexts['Professions'][MyRealm][MyName]['Class'] = MyClass

	self:RegisterEvent('PLAYER_ENTERING_WORLD')
	self:RegisterEvent('SPELLS_CHANGED')
	self:RegisterEvent('TRADE_SKILL_UPDATE')
	self:SetScript('OnUpdate', OnUpdate)
	self:SetScript('OnEvent', OnEvent)
	self:Update(1)
end

local Disable = function(self)
	self.SecureButton:Hide()
	self.SecureSet = false
	self.Text:SetText('')
	self:UnregisterAllEvents()
	self:SetScript('OnEvent', nil)
	self:SetScript('OnEnter', nil)
	self:SetScript('OnLeave', nil)
	self:SetScript('OnUpdate', nil)
end

DataText:Register('Professions', Enable, Disable, OnEvent)