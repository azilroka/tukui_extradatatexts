local T, C, L = Tukui:unpack()
local AddOnName, Engine = ...
local AddOn = {}
Engine[1] = AddOn
--TEDT = Engine

local sort, pairs = sort, pairs

function AddOn:OrderedPairs(t, f)
	local function orderednext(t, n)
		local key = t[t.__next]
		if not key then return end
		t.__next = t.__next + 1
		return key, t.__source[key]
	end

	local keys, kn = {__source = t, __next = 1}, 1
	for k in pairs(t) do
		keys[kn], kn = k, kn + 1
	end
	sort(keys, f)
	return orderednext, keys
end

AddOn.MyRealm = GetRealmName()
AddOn.MyName = UnitName('player')
AddOn.MyClass = select(2, UnitClass('player'))
AddOn.DataTexts = T['DataTexts']
AddOn.L = L
AddOn.RGBToHex = T.RGBToHex
AddOn.Comma = T.Comma

local Title, Version = GetAddOnMetadata(AddOnName, "Title"), GetAddOnMetadata(AddOnName, "Version")

function AddOn:Print(string)
	DEFAULT_CHAT_FRAME:AddMessage(format("%s %s: %s", Title, Version, string))
end

TukuiExtraDataTexts = {}
