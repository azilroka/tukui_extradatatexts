## Interface: 60000
## Title: |cffC495DDTukui|r Extra DataTexts
## Author: Azilroka, Hydra
## Version: 1.01
## Notes: DataTexts for Tukui 16
## OptionalDeps: DataStore_Characters, Tukui, DuffedUI
## SavedVariables: TukuiExtraDataTexts

Init.lua
Locales.xml
DataTexts.xml