﻿local EDT = unpack(select(2,...))
local L = EDT.L

------------------------------------------------
-- Data Text Locales
------------------------------------------------

L.DataText.Current = 'Current:'
L.DataText.Weekly = 'Weekly:'
L.DataText.GoldReset = 'Gold has been reset!'
L.DataText.TimeReset = 'Time Played has been reset!'
L.DataText.ProfessionsReset = 'Professions has been reset!'
L.DataText.ResetInfo = '|cFFAAAAAAReset Info: Hold Shift + Right Click|r'
L.DataText.Node = 'Node'
L.DataText.Nodes = 'Nodes'
L.DataText.TimePlayed = 'Time Played'
L.DataText.RealmTimePlayed = 'Realm Time Played'
L.DataText.PreviousLevel = 'Previous Level'
L.DataText.NoProfession = 'No Profession'
L.DataText.SpecLoot = 'Spec & Loot: '
L.DataText.Spec = 'Spec: '
L.DataText.Loot = 'Loot: '
L.DataText.Equipped = 'Equipped'
L.DataText.BankedMissingItem = 'Banked/Missing Item'
L.DataText.NoEquipmentSets = 'You do not have any Equipment Sets in Equipment Manager.\nIf you are certain you have sets then relog.'
L.DataText.EquipmentSet = 'Equipment Set'
L.DataText.UnreadMail = 'Unread Mail'
L.DataText.ReadMail = 'Read Mail'

------------------------------------------------
-- Tooltips Locales
------------------------------------------------
