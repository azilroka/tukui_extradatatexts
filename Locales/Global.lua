﻿local EDT = unpack(select(2, ...))
local L = EDT.L
local format, gsub = format, gsub

------------------------------------------------
-- Data Text Locales
------------------------------------------------

L.DataText.TextureString = '|T%s:20:20:0:0:64:64:4:60:4:60|t'

------------------------------------------------
-- Tooltips Locales
------------------------------------------------

L.Tooltips.TextureString = '|T%s:14:14:0:0:64:64:4:60:4:60|t'
L.Tooltips.NoMoney = format('%s %s', NO, MONEY)